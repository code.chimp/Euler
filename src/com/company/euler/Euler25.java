package com.company.euler;

import java.math.BigInteger;
import java.util.ArrayList;

public class Euler25 {
    public static void main(String[] args) {
        ArrayList<BigInteger> fibs = new ArrayList<>(500);

        fibs.add(0, BigInteger.ZERO);
        fibs.add(1, BigInteger.ONE);

        int idx = 2;
        while (true) {
            BigInteger nextVal = fibs.get(idx - 1).add(fibs.get(idx - 2));
            fibs.add(idx, nextVal);

            if (nextVal.toString().length() == 1000) {
                System.out.println(nextVal.toString());
                break;
            }
            idx++;

            if (idx % 100 == 0)
             System.out.println(String.format("%d", idx));
        }
        System.out.println("index is " + idx);
    }
}