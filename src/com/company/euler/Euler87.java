package com.company.euler;

import org.apache.commons.math3.primes.Primes;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class Euler87 {

    public static void main(String[] args) {
        final int Max = 50000000;

        int[] squares = new int[7071];
        int[] cubes = new int[7071];
        int[] quads = new int[7071];

        List<Integer> primes = new LinkedList<>();

        for (int prime=2; prime <= 7071; prime++) {
            if (!Primes.isPrime(prime)) continue;

            primes.add(prime);
            squares[prime] = prime * prime;
            cubes[prime] = squares[prime] * prime;
            quads[prime] = cubes[prime] * prime;
        }

        HashSet<Integer> set = new HashSet<>();
        for (int quad = 0; quad <= primes.size(); quad++) {
            for (int cube = 0; cube <= primes.size(); cube++) {
                for (int square = 0; square <= primes.size(); square++) {
                    int sum = quads[quad] + cubes[cube] + squares[square];

                    if (sum >= Max) break;
                    set.add(sum);
                }
            }
        }
        System.out.println(set.size());
    }
}
