package com.company.euler;

import java.util.Arrays;
import java.util.stream.IntStream;

/*
 Input: an integer n > 1.

 Let A be an array of Boolean values, indexed by integers 2 to n,
 initially all set to true.

 for i = 2, 3, 4, ..., not exceeding √n:
   if A[i] is true:
     for j = i2, i2+i, i2+2i, i2+3i, ..., not exceeding n:
       A[j] := false.

 Output: all i such that A[i] is true.
 */
public class Euler10 {
    static final int end = 2000000;
    static Boolean[] primes = new Boolean[end];

    public static void main(String[] main) {
        Arrays.fill(primes, true);
        primes[0] = false;
        primes[1] = false;

        for (int pos=2;pos < Math.sqrt(end);) {
            for (int k = 2, i = k * pos ; i < end; i = pos * k++)
                primes[i] = false;

            while (pos++ < end && primes[pos] != true);
        }

        long sumPrimes = IntStream.range(0, primes.length)
                .mapToLong(idx -> primes[idx]?idx:0)
                .sum();

        System.out.println(sumPrimes);
    }
}
