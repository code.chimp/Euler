package com.company.euler;

import java.util.HashMap;

/**
 * Created by rogerwarner on 1/4/17.
 */
public class Euler35 {

    public static void main(String[] args) {

        HashMap<Integer, Integer> seenit = new HashMap<>();

        int sum = 0;
        for (int i = 0; i < 1000; i++) {
            if (seenit.containsKey(i) == false && i % 3 == 0) {
                sum += i;
                seenit.put(i,i);
            }
            if (seenit.containsKey(i) == false && i % 5 == 0) {
                sum += i;
                seenit.put(i,i);
            }

        }

        System.out.println("Answer is: " + sum);
    }
}
