package com.company.euler;

/**
 * Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
  *  74  75  76  77  78  79  80  81  82
  *  73  43  44  45  46  47  48  49  50                      b
  *  72  42  21  22  23  24  25  26  51
  *  71  41  20   7   8   9  10  27  52
  *  70  40  19   6   1   2  11  28  54
  *  69  39  18   5   4   3  12  29  55
  *  68  38  17  16  15  14  13  30  56
  *  67  37  36  35  34  33  32  31  57
  *  66  65  64  63  62  61  60  59  58
 *
 * It can be verified that the sum of the numbers on the diagonals is 101.
 *
 * What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
 */
public class Euler28 {
    public static void main(String ... args) {
        final int max = 1001;
        var sum = 0;
        for (int i = 1; i<=Math.ceil(max/2); i++) {
            sum+=i;

        }
    }
}
