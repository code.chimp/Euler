package com.company.euler;

import java.math.BigDecimal;
import java.util.Arrays;

public class Euler20 {
    public static void main(String[] args) {
        new Euler20().go(100);
    }

    public void go(long n) {
        BigDecimal fact = new BigDecimal(1);
        while (n >= 1) {
            fact = fact.multiply(new BigDecimal(n));
            n--;
        }

        System.out.println(fact);
        System.out.println(String.valueOf(fact).chars().map(c->c-'0').sum());
    }
}
