package com.company.euler;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.HashSet;

public class Euler19 {

    public static final void main(String[] main) {

        new Euler19().go();
    }

    public void go() {
        // day offsets from 1900
        HashSet<Integer> mondaySet = new HashSet<>();
        for (int monday = 1; monday < 1900 + 100 * 366; monday += 7) {
            mondaySet.add(monday);
        }

        // day offsets from 1900
        HashSet<Integer> monthSet = new HashSet<>();
        int day = 1;
        for (int year = 1900; year < 2001; year++) {
            for (int month = 0; month < 12; month++, day += dayfun(year, month)) {
                monthSet.add(day);
            }
        }

        System.out.println(Arrays.asList(
                Sets.intersection(mondaySet, monthSet).stream().filter(x -> x>365).toArray(Integer[]::new)));
        System.out.println(Sets.intersection(mondaySet, monthSet).stream().filter(x -> x>365).count());

    }

    private int dayfun(int year, int month) {
        switch (month % 12) {
            case 9:
            case 4:
            case 6:
            case 11:
                return 30;
            case 2:
                if ((year) % 4 == 0 && year < 2000)
                    return 29;
                else
                    return 28;
            default:
                return 31;
        }
    }
}
