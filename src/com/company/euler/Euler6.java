package com.company.euler;

import java.util.stream.IntStream;

public class Euler6 {
    public static void main(String args[]) {
        int sum = IntStream.rangeClosed(1, 100).sum();
        long sumSquared = (long) Math.pow(sum, 2);
        long squaredSum = IntStream.rangeClosed(1, 100).map(i -> i * i).sum();

        System.out.println(String.format("%d %d %d ", sumSquared, squaredSum, sumSquared - squaredSum));
    }
}