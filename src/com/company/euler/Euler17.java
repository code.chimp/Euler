package com.company.euler;

public class Euler17 {
    public static void main(String[] args) {
        try {
            new Euler17().go();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }
    }

    public void go() throws Exception {
        int max = 0;
        for (int x = 1; x <= 1000; x++) {
            StringBuilder buf = new StringBuilder();
            String andFlag = "";
            int digits = x;
            while (digits > 0) {

                if (digits == 1000) {
                    buf.append("ONETHOUSAND");
                    break;
                } if (digits >= 100) {
                    int msd = (int)(x / Math.pow(10, (int)Math.log10(x)));
                    buf.append(numTo100s(msd));
                    digits %= 100;
                    andFlag = "AND";

                } else if (digits < 100 && digits >= 20) {
                    buf.append(andFlag);  andFlag = "";
                    buf.append(numTo10s(digits/10));
                    if ((digits % 10) == 0) break;
                    digits %= 10;

                } else if (digits < 20 && digits >= 10) {
                    buf.append(andFlag);
                    buf.append(numToTeen(digits));
                    break;

                } else {
                    buf.append(andFlag);
                    buf.append(digToWord(digits));
                    if (digits <= 10)
                        digits=0;
                }
            }
            max += buf.length();
            System.out.println(buf.toString());
        }
        System.out.println(max);
    }

    public String numToTeen(int digs) throws Exception {
        switch (digs) {
            case 10:
                return "TEN";
            case 11:
                return "ELEVEN";
            case 12:
                return "TWELVE";
            case 13:
                return "THIRTEEN";
            case 14:
                return "FOURTEEN";
            case 15:
                return "FIFTEEN";
            case 16:
                return "SIXTEEN";
            case 17:
                return "SEVENTEEN";
            case 18:
                return "EIGHTEEN";
            case 19:
                return "NINETEEN";
            default:
                throw new Exception("bad dig " + digs);
        }
    }

    public String numTo10s(int dig) throws Exception {
        switch (dig) {
            case 2:
                return "TWENTY";
            case 3:
                return "THIRTY";
            case 4:
                return "FORTY";
            case 5:
                return "FIFTY";
            case 6:
                return "SIXTY";
            case 7:
                return "SEVENTY";
            case 8:
                return "EIGHTY";
            case 9:
                return "NINETY";
            default:
                return "";
        }
    }

    public String numTo100s(int dig) throws Exception {
        return digToWord(dig) + "HUNDRED";
    }

    public String digToWord(int dig) throws Exception {
        switch (dig) {
            case 1:
                return "ONE";
            case 2:
                return "TWO";
            case 3:
                return "THREE";
            case 4:
                return "FOUR";
            case 5:
                return "FIVE";
            case 6:
                return "SIX";
            case 7:
                return "SEVEN";
            case 8:
                return "EIGHT";
            case 9:
                return "NINE";
            case 0:
                return "";
            default:
                throw new Exception("bad single dig" + dig);
        }
    }
}
