package com.company.euler;

import java.util.LinkedList;
import java.util.List;

/**
 * Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
 * <p>
 * 1634 = 14 + 64 + 34 + 44
 * 8208 = 84 + 24 + 04 + 84
 * 9474 = 94 + 44 + 74 + 44
 * As 1 = 14 is not a sum it is not included.
 * <p>
 * The sum of these numbers is 1634 + 8208 + 9474 = 19316.
 * <p>
 * Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
 */
public class Euler30 {

    public static void main(String[] args) {
        new Euler30().go();
    }

    public void go() {
        var bigsum = 0L;
        for (int num = 1000; num <= Integer.MAX_VALUE; num++) {
            var m = num;
            var sum = 0L;
            var list = new LinkedList<Integer>();
            while(m > 0) {
                var dig = m % 10;
                m = m / 10;

                sum += (long)Math.pow(dig, 5);
                list.add(dig);
            }

            if (sum == num) {
                System.out.println("" + num + " + " +sum+":"+list.toString());
                System.out.println(bigsum+=sum);
            }
        }

        System.out.println();

    }
}
