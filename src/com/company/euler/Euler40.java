package com.company.euler;

public class Euler40 {

    public static void main(String[] args) {
        StringBuilder buf = new StringBuilder(1000000000);

        for (int pos = 1; pos < 1000000; pos++) {
            buf.append(pos);
        }

        int prod = 1;
        for (int i = 1; i <= 1000000; i *=10) {
            System.out.println(String.format("idx %d, num %d", i, buf.charAt(i - 1) - '0'));
            prod *= buf.charAt(i - 1) - '0';
        }
        System.out.println("prod " + prod);
    }
}
