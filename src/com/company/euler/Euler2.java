package com.company.euler;

public class Euler2 {

    public static void main(String[] args) {

        final int max = 4 * 1000 * 1000;
        long[] fibs = new long[max];
        long fibSum = 0;
        fibs[0] = 0;
        fibs[1] = 1;

        for (int i = 2; ; i++) {
            long temp = fibs[i - 1] + fibs[i - 2];
            if (temp > max)
                break;
            fibs[i] = temp;
            fibSum += temp % 2 == 0 ? 0 : temp;
        }

        System.out.println(fibSum+1);
    }

}
