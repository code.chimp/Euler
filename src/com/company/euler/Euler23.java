package com.company.euler;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example,
 * the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
 * <p>
 * A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant
 * if this sum exceeds n.
 * <p>
 * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written
 * as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater
 * than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
 * <p>
 * Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
 */
public class Euler23 {

    public static void main(String... args) {
        var out = new Euler23().go();

        System.out.println(out);
    }

    private int go() {
        var list = new TreeSet<Integer>(abundants()).toArray(new Integer[0]);
        var notsummed = new TreeSet<Integer>();

        next:
        for (int num = 1; num < 28123 ; num++) {
            for (int i: list) {
                if (i>num)break;
                for (int j: list) {
                    if (j>num) break;
                    var sum = i+j;
                    if (sum == num)
                        continue next;
                }
            }
            notsummed.add(num);

            if (num % 10 == 0)
                System.out.println(num);
        }

        return  notsummed.stream().mapToInt(Integer::intValue).sum();
    }

    private List abundants() {
        var abondanza = new LinkedList<Integer>();
        for (int num = 1; num <= 28123; num++) {
            var sum = calculateDivisorSum(num);

            if (sum <= num) continue;
            abondanza.add(num);
        }
        return abondanza;
    }

    private int calculateDivisorSum(int num) {
        var list = new LinkedList<Integer>();

        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0)
                list.add(i);
        }

        return list.stream().mapToInt(Integer::intValue).sum();
    }
}
