package com.company.euler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Euler61 {

    public static void main(String[] args) {
        new Euler61().BruteForce();
    }

    Integer[] solution;
    Integer[][] numbers;

    public void BruteForce() {
        int result = 0;
        solution = new Integer[6];


        numbers = new Integer[6][];

        for (int i = 0; i < 6; i++) {
            numbers[i] = generateNumbers(i);
        }

        for (int i = 0; i < numbers[5].length; i++) {
            solution[5] = numbers[5][i];
            if (FindNext(5, 1)) break;
        }
        //result = solution.Sum();
        result = Arrays.stream(solution).mapToInt(Integer::intValue).sum();

        WriteStatus();
        System.out.println("The sum of the series is " + result);
    }


    public boolean FindNext(int last, int length) {
        for (int i = 0; i < solution.length; i++) {
            if (solution[i]  != null && solution[i] != 0) continue;
            for (int j = 0; j < numbers[i].length; j++) {

                boolean unique = true;
                for(int k = 0; k < solution.length; k++){
                    if (numbers[i][j] == solution[k]) {
                        unique = false;
                        break;
                    }
                }

                if ( unique && ((numbers[i][j] / 100) == (solution[last] % 100))) {
                    solution[i] = numbers[i][j];
                    if (length == 5) {
                        if (solution[5] / 100 == solution[i] % 100) return true;
                    }
                    if (FindNext(i, length + 1)) return true;
                }
            }
            solution[i] = 0;
        }
        return false;
    }

    private void WriteStatus(){
        System.out.println(String.format("%s", solution.toString()));
    }


    public Integer[] generateNumbers(int type) {

        List<Integer> numbers = new LinkedList<>();

        int n = 0;
        int number = 0;

        while (number < 10000) {
            n++;
            switch (type) {

                case 0:
                    //Triangle numbers
                    number = n * (n + 1) / 2;
                    break;
                case 1:
                    // Square numbers
                    number = n * n;
                    break;
                case 2:
                    // Pentagonal numbers
                    number = n * (3 * n - 1) / 2;
                    break;
                case 3:
                    //Hexagonal numbers
                    number = n * (2*n - 1);
                    break;
                case 4:
                    //Heptagonal numbers
                    number = n * (5 * n - 3) / 2;
                    break;
                case 5:
                    //Octagonal numbers
                    number = n * (3 * n - 2);
                    break;
            }
            if (number > 999)
                numbers.add(number);
        }

        Integer[] a = numbers.toArray(new Integer[0]);
        return a;
    }
}