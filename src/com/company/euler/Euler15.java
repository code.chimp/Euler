package com.company.euler;

import java.util.Arrays;

public class Euler15 {
    final int SIZE = 20;
    long[][] pts = new long[SIZE+1][SIZE+1];

    public static void main(String[] args) {
        new Euler15().go();
    }

    public void go() {
        for (long[] arr : pts)
            Arrays.fill(arr, 1L);


        for (int i = SIZE - 1; i >= 0; i--) {
            for (int j = SIZE - 1; j >= 0; j--) {
                pts[i][j] = pts[i + 1][j] + pts[i][j + 1];
            }
        }

        System.out.println(pts[0][0]);
    }
}
