package com.company.euler;

import java.util.*;

/*
Problem 24
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the
digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it
lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/
public class Euler24 {

    public static void main(String... args) {
        new Euler24().go();
    }

    public void go() {
        TreeSet<String>tree = new TreeSet(permutationFinder("0123456789"));

        String s="-0";
        int idx=0;
        for (Iterator<String> i = tree.iterator(); i.hasNext() && idx < 1000000;idx++) {
            s = i.next();
        }

        System.out.println(idx+":"+s);
    }

    public static Set<String> permutationFinder(String str) {
        Set<String> perm = new TreeSet<String>();
        //Handling error scenarios
        if (str == null) {
            return null;
        } else if (str.length() == 0) {
            perm.add("");
            return perm;
        }
        char initial = str.charAt(0); // first character
        String rem = str.substring(1); // Full string without first character
        Set<String> words = permutationFinder(rem);
        for (String strNew : words) {
            for (int i = 0;i<=strNew.length();i++){
                perm.add(charInsert(strNew, initial, i));
            }
        }
        return perm;
    }

    public static String charInsert(String str, char c, int j) {
        String begin = str.substring(0, j);
        String end = str.substring(j);
        return begin + c + end;
    }
}
