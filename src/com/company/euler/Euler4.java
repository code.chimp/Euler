package com.company.euler;

public class Euler4 {
    public static void main(String[] args) {
        int maxi = 0;
        for (int i = 100; i <= 999; i++) {
            for (int j = 100; j<999; j++) {
                String t = String.valueOf(i * j);

                String first = new StringBuffer(t.substring(0, t.length() / 2)).reverse().toString();
                String second = new StringBuffer(t.substring(t.length() / 2)).reverse().toString();

//                System.out.println(String.format("%s flipped %s", t, second.concat(first)));

                if (second.concat(first).equals(t)) {
                    maxi = Math.max(maxi, Integer.parseInt(t));
                    System.out.println("new max " + maxi);
                }
            }
        }
    }
}
