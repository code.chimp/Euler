package com.company.euler;

import org.apache.commons.math3.primes.Primes;

public class Euler37 {

    public static void main(String[] args) {
	// write your code here
        int i = 10;
        int found = 0;
        int sum = 0;

        while (i++ < Integer.MAX_VALUE && found < 11) {
            if (!Primes.isPrime(i)) continue;

            String prime = "" + i;

            if (isAllLeftPrime(prime) && isAllRightPrime(prime)){
                System.out.println(String.format("Found %d %s ", found++, prime));
                sum+=i;
            }
        }
        System.out.println("sum " + sum);
    }

    private static boolean isAllLeftPrime(String prime) {
        if (Primes.isPrime(Integer.parseInt(prime)))
            if (prime.length() == 1)
                return true;
            else if (isAllLeftPrime(prime.substring(0, prime.length() - 1)))
                return true;

        return false;
    }

    private static boolean isAllRightPrime(String prime) {
        if (Primes.isPrime(Integer.parseInt(prime)))
            if (prime.length() == 1)
                return true;
            else if (isAllRightPrime(prime.substring(1, prime.length())))
                return true;

        return false;
    }
}
